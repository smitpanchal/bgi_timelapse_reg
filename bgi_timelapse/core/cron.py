import razorpay
from decouple import config
from .models import (
    Order,
)


def razorpay_checks(request):
    # ----- CONNECTING TO RAZORPAY CLIENT ----- #
    if config("DOMAIN_NAME") == "prod":
        client = razorpay.Client(
            auth=(
                config("RAZORPAY_PUBLIC_KEY_PRODUCTION"),
                config("RAZORPAY_SECRET_KEY_PRODUCTION")
            )
        )

    else:
        client = razorpay.Client(
            auth=(
                config("RAZORPAY_PUBLIC_KEY_DEVELOPMENT"),
                config("RAZORPAY_SECRET_KEY_DEVELOPMENT")
            )
        )
    # ----- CONNECTING TO RAZORPAY CLIENT ----- #

    # print("===> request.GET: ", request.GET)
    # print("===> request.GET.get('key'): ", request.GET.get('key'))
    # if request.GET.get('key') == config("RAZORPAY_CHECK_KEY"):

    qry_orders = Order.objects.all()

    data = list()
    for qry_order in qry_orders:
        # temp = dict()
        order_id = qry_order.order_payment_id

        order = client.order.fetch(order_id)

        if order.get("status") == "paid":
            # MARKING is_paid TRUE FOR ALL RECEIVED PAYMENTS
            qry_order.is_paid = True
            qry_order.save()
            # MARKING is_paid TRUE FOR ALL RECEIVED PAYMENTS OF REGISTERD-USERS
            if not qry_order.register_user.is_paid:
                qry_order.register_user.is_paid = True
                qry_order.register_user.save()
                data.append(qry_order.register_user.id)
            # temp['user_id'] = qry_order.register_user.id
            # temp['mobile_number'] = qry_order.register_user.mobile_number
            # temp['status'] = True
            # print(f"Payment received for {qry_order}")
        # else:
            # temp['user_id'] = qry_order.register_user.id
            # temp['mobile_number'] = qry_order.register_user.mobile_number
            # temp['status'] = False
            # print(f"Payment not received for {qry_order}")
    print("====> is_paid Status is changed for user_ids: ", data)
