from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    gst = models.IntegerField(default=0)
    final_amount = models.DecimalField(
        max_digits=10, decimal_places=2, default=0)
    number_of_people = models.IntegerField(default=1)

    is_approved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = 'category'

    def __str__(self) -> str:
        return self.name


class RegisterUser(models.Model):
    name = models.CharField(max_length=255)
    proof = models.ImageField(upload_to='bgitimelapse/user/')
    location = models.CharField(max_length=255, default="")
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True)
    mobile_number = models.CharField(max_length=15)
    email_address = models.CharField(max_length=50)
    event_loc = models.CharField(max_length=55)

    is_paid = models.BooleanField(default=False)
    is_gift = models.BooleanField(default=False)
    term_condition = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = 'register_user'

    def __str__(self) -> str:
        return self.name


class Order(models.Model):
    invoice_no = models.PositiveBigIntegerField(default=0)
    register_user = models.ForeignKey(
        RegisterUser, on_delete=models.SET_NULL, null=True
    )
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True
    )
    order_payment_id = models.CharField(max_length=255)
    razorpay_payment_id = models.CharField(max_length=255, null=True)
    razorpay_signature = models.TextField(null=True)
    is_paid = models.BooleanField(default=False)
    amount = models.IntegerField()
    create_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'order'

    def __str__(self) -> str:
        return self.register_user.mobile_number


class RegisterUserAddOn(models.Model):
    name = models.CharField(max_length=255)
    proof = models.ImageField(upload_to='bgitimelapse/user/')
    registered_by = models.ForeignKey(
        RegisterUser, on_delete=models.SET_NULL, null=True)
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_delete = models.BooleanField(default=False)

    class Meta:
        db_table = 'register_user_add_on'

    def __str__(self) -> str:
        return f'{self.name} - {self.registered_by.name}'
