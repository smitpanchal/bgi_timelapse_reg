from django.urls import path
from .views import (
    bhopal_index_view, indore_index_view,
    get_category_based_details,
    user_registration, callback_url, bhopal, success_page,
    indore2, term_condition, indore_new, blog, How_To_Register_For_BGITimelapse, Farhan_Akhtar_Live_At_BGITimelapse, 
    Win_Exciting_Prizes_at_BGI_Timelapse, What_is_BGITimelapse, Get_Ready_To_Groove_With_Farhan_Akhtar, 
    Schedule_And_Events_At_BGITimelapse, Top_5_College_Fests_You_Must_Attend,
    razorpay_checks
)

urlpatterns = [
    path('', bhopal_index_view, name='bhopal_index_view'),
    path('indore_reg/', indore_index_view, name='indore_index_view'),
    path('get_category_based_details/<int:id>',
         get_category_based_details, name='get_category_based_details'),
    path('user_registration', user_registration, name='user_registration'),
    path('callback_url', callback_url, name='callback_url'),
    path('success_page', success_page, name='success_page'),

    # Extra pages
    path('bhopal/', bhopal, name='bhopal'),
    path('indore/', indore2, name='indore2'),
    path('term_condition', term_condition, name='term_condition'),
    path('indore_new', indore_new, name='indore_new'),
    path('blog', blog, name='blog'),
    path('How-To-Register-For-BGITimelapse', How_To_Register_For_BGITimelapse, name='How-To-Register-For-BGITimelapse'),
    path('Farhan-Akhtar-Live-At-BGITimelapse', Farhan_Akhtar_Live_At_BGITimelapse, name='Farhan-Akhtar-Live-At-BGITimelapse'),
    path('Win-Exciting-Prizes-at-BGI-Timelapse', Win_Exciting_Prizes_at_BGI_Timelapse, name='Win-Exciting-Prizes-at-BGI-Timelapse'),
    path('What-is-BGITimelapse', What_is_BGITimelapse, name='What-is-BGITimelapse'),
    path('Get-Ready-To-Groove-With-Farhan-Akhtar', Get_Ready_To_Groove_With_Farhan_Akhtar, name='Get-Ready-To-Groove-With-Farhan-Akhtar'),
    path('Schedule-And-Events-At-BGITimelapse', Schedule_And_Events_At_BGITimelapse, name='Schedule-And-Events-At-BGITimelapse'),
    path('Top-5-College-Fests-You-Must-Attend', Top_5_College_Fests_You_Must_Attend, name='Top-5-College-Fests-You-Must-Attend'),

    # PATH FOR RAZORPAY PAYMENT CHECK
    path('razorpay_checks', razorpay_checks, name='razorpay_checks'),
]
