from django.contrib import admin
from import_export.admin import ExportMixin, ImportExportModelAdmin
from .models import (
    Category, Order, RegisterUser, RegisterUserAddOn
)


class UserAddonAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'registered_by')


class UserAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_filter = ('created_at', 'is_paid')
    list_display = ('id', 'name', 'mobile_number', 'category', 'event_loc',
                    'number_of_people', 'is_paid', 'add_on', 'created_at')

    def number_of_people(self, obj):
        return obj.category.number_of_people

    def created_at(self, obj):
        return obj.created_at

    def add_on(self, obj):
        return list(RegisterUserAddOn.objects.filter(registered_by=obj.id).values_list('name', flat=True))


# Register your models here.
admin.site.register(Category)
admin.site.register(RegisterUser, UserAdmin)
admin.site.register(Order)
admin.site.register(RegisterUserAddOn, UserAddonAdmin)
