import json
import razorpay
from decouple import config
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.contrib import messages
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail
from .models import (
    Category, RegisterUser, Order,
    RegisterUserAddOn
)


def bhopal_index_view(request):
    """
    purpose  : Render index form page
    request  : request
    response : Render
    """
    # messages.warning(
    #     request,
    #     "Online registration for Bhopal Location has been closed Please contact on-spot hub for registration (BIST Main Ground)!"
    # )
    # return redirect(indore_index_view)
    request.session['event_loc'] = "Bhopal"
    qry_category = Category.objects.filter(
        is_approved=True,
        is_delete=False,
    ).all()
    context = {
        'category': qry_category,
    }
    return render(request, 'core/index_bhopal.html', context=context)


def indore_index_view(request):
    """
    purpose  : Render index form page
    request  : request
    response : Render
    """
    request.session['event_loc'] = "Indore"
    qry_category = Category.objects.filter(
        is_approved=True,
        is_delete=False
    ).all()
    context = {
        'category': qry_category,
    }
    # messages.warning(
    #     request,
    #     "Online registration for Indore Location has been closed Please contact on-spot hub for registration (Sushila Devi Bansal College)!"
    # )
    # return redirect(bhopal_index_view)
    return render(request, 'core/index_indore.html', context=context)


@csrf_exempt
def get_category_based_details(request, id):
    """
    purpose  : Fetch Category data so amount will be shown automatically
    request  : id
    response : JsonResponse of category's details
    """
    qry = Category.objects.filter(
        id=id
    ).first()

    return JsonResponse(
        {
            'number_of_people': qry.number_of_people,
            'amount': qry.final_amount
        }
    )


@csrf_exempt
def user_registration(request):
    """
    purpose  : Form submission with payment integration
    request  : request form-data
    response : redirection to payment gateway
    """

    # print("====> request.POST: ", request.POST)
    # print("====> request.FILES: ", request.FILES)
    name_list = dict(request.POST)['name']
    # file_list = dict(request.FILES)['proof']

    qry_category = Category.objects.get(
        id=int(request.POST['category'])
    )

    # ----- REGISTERING MAIN USER WHICH IS ENTERED FIRST ----- #
    qry_register_user = RegisterUser.objects.create(
        name=name_list[0],
        # location=request.POST['city'],
        mobile_number=request.POST['mobile_number'],
        email_address=request.POST['email_address'],
        # proof=file_list[0],
        category=qry_category,
        event_loc=request.session['event_loc'] if 'event_loc' in request.session else "Bhopal"
    )
    # ----- REGISTERING MAIN USER WHICH IS ENTERED FIRST ----- #

    # ----- REGISTERING ADDON USERS WHICH IS ENTERED AFTER ----- #
    for i in range(1, len(name_list)):
        RegisterUserAddOn.objects.create(
            name=name_list[i],
            # proof=file_list[i],
            category=qry_category,
            registered_by=qry_register_user
        )
    # ----- REGISTERING ADDON USERS WHICH IS ENTERED AFTER ----- #

    amount = qry_category.final_amount

    # ----- CONNECTING TO RAZORPAY CLIENT ----- #
    if config("DOMAIN_NAME") == "prod":
        client = razorpay.Client(
            auth=(
                config("RAZORPAY_PUBLIC_KEY_PRODUCTION"),
                config("RAZORPAY_SECRET_KEY_PRODUCTION")
            )
        )

    else:
        client = razorpay.Client(
            auth=(
                config("RAZORPAY_PUBLIC_KEY_DEVELOPMENT"),
                config("RAZORPAY_SECRET_KEY_DEVELOPMENT")
            )
        )
    # ----- CONNECTING TO RAZORPAY CLIENT ----- #

    # ----- CREATING ORDER FROM RAZORPAY ----- #
    razorpay_order = client.order.create(
        {
            "amount": int(amount) * 100,
            "currency": "INR",
            "payment_capture": "1",
        }
    )
    # ----- CREATING ORDER FROM RAZORPAY ----- #

    qry_order = Order.objects.create(
        register_user=qry_register_user,
        category=qry_category,
        order_payment_id=razorpay_order["id"],
        amount=amount,
    )

    # ----- RENDERING RAZORPAY PAGE AS PER DOMAIN NAME ----- #
    if config("DOMAIN_NAME") == "prod":
        return render(
            request,
            "payment/payment.html",
            {
                "callback_url": config("DOMAIN_URL") + "/callback_url",
                "razorpay_key": config("RAZORPAY_PUBLIC_KEY_PRODUCTION"),
                "order": qry_order,
                "email": request.POST['email_address'],
                "mobile_number": request.POST['mobile_number'],
                "name": qry_register_user.name
            },
        )
        # messages.success(request, 'Registration is successful!')

    elif config("DOMAIN_NAME") == "dev":
        return render(
            request,
            "payment/payment.html",
            {
                "callback_url": config("DOMAIN_URL") + "/callback_url",
                "razorpay_key": config("RAZORPAY_PUBLIC_KEY_DEVELOPMENT"),
                "order": qry_order,
                "email": request.POST['email_address'],
                "mobile_number": request.POST['mobile_number'],
                "name": qry_register_user.name
            },
        )
        # messages.success(request, 'Registration is successful!')

    else:
        return render(
            request,
            "payment/payment.html",
            {
                "callback_url": config("DOMAIN_URL") + "/callback_url",
                "razorpay_key": config("RAZORPAY_PUBLIC_KEY_DEVELOPMENT"),
                "order": qry_order,
                "email": request.POST['email_address'],
                "mobile_number": request.POST['mobile_number'],
                "name": qry_register_user.name
            },
        )
        # messages.success(request, 'Registration is successful!')
    # ----- RENDERING RAZORPAY PAGE AS PER DOMAIN NAME ----- #

    # return redirect('/registration/marathon')


def success_page(request):
    return render(request, 'payment/success.html')


@csrf_exempt
def callback_url(request):
    """
    purpose  : To mark successful or failed payments
    request  : request data
    response : redirection
    """
    def verify_signature(response_data):
        if config("DOMAIN_NAME") == "prod":
            client = razorpay.Client(
                auth=(
                    config("RAZORPAY_PUBLIC_KEY_PRODUCTION"),
                    config("RAZORPAY_SECRET_KEY_PRODUCTION")
                )
            )
            return client.utility.verify_payment_signature(response_data)

        else:
            client = razorpay.Client(
                auth=(
                    config("RAZORPAY_PUBLIC_KEY_DEVELOPMENT"),
                    config("RAZORPAY_SECRET_KEY_DEVELOPMENT")
                )
            )
            return client.utility.verify_payment_signature(response_data)

    if "razorpay_signature" in request.POST:
        order_payment_id = request.POST.get("razorpay_order_id", "")
        razorpay_payment_id = request.POST.get("razorpay_payment_id", "")
        razorpay_signature = request.POST.get("razorpay_signature", "")

        qry_order = Order.objects.filter(
            order_payment_id=order_payment_id
        ).first()

        if qry_order:
            qry_order.razorpay_payment_id = razorpay_payment_id
            qry_order.razorpay_signature = razorpay_signature
            qry_order.save()
        else:
            redirect(bhopal_index_view)

        """
            -> If signature is verified then only make is_paid True in
               database otherwise make it False.
        """
        if verify_signature(request.POST):
            qry_last_invoice_no = Order.objects.order_by('invoice_no').last()
            if qry_last_invoice_no:
                invoice_no = qry_last_invoice_no.invoice_no + 1
            else:
                invoice_no = 1

            qry_order.is_paid = True
            qry_order.invoice_no = invoice_no
            qry_order.save()

            qry_reg_user = RegisterUser.objects.filter(
                id=qry_order.register_user_id
            ).last()
            qry_reg_user.is_paid = True
            qry_reg_user.save()

            try:
                if qry_reg_user.event_loc == "Bhopal":
                    template = 'registration_bhopal.html'
                else:
                    template = 'registration_indore.html'

                subject = 'Congratulations 🥳 You have successfully registered for the BGI Timelapse'
                html_message = render_to_string(
                    f'mail/{template}',
                    {
                        'id': qry_reg_user.id,
                        'username': qry_reg_user.name,
                        'category': qry_reg_user.category.name,
                        'no_of_people': qry_reg_user.category.number_of_people,
                        'price': qry_reg_user.category.final_amount,
                    }
                )
                plain_message = strip_tags(html_message)

                send_mail(
                    subject,
                    plain_message,
                    settings.EMAIL_HOST_USER,
                    [qry_reg_user.email_address],
                    html_message=html_message,
                    fail_silently=False
                )
            except Exception as ex:
                print("===> Error from sending sms_for_marathon email: ", ex)
                pass

            # Sending SMS for purchase plan
            # try:
            #     marathon_category_name = qry_reg_user.marathon_category.name
            #     marathon_category_amount = qry_reg_user.marathon_category.amount
            #     template_id = settings.ZESTWINGS_SMS

            #     url = "https://sms.zestwings.com/smpp.sms?username=" + \
            #           settings.ZESTWINGS_USERNAME + "&password=" + \
            #           settings.ZESTWINGS_PASSWORD + "&to=" + \
            #           str(qry_reg_user.mobile_number) + "&from=" + \
            #           settings.ZESTWINGS_FROM + "&text=Dear Participant, Congratulations on successfully " \
            #                                     "registering for Pankh MP Half Marathon in " + \
            #           marathon_category_name + " category, powered by ToneOP with a payment of Rs. " + \
            #           str(int(
            #               marathon_category_amount
            #           )) + ". Further details will be shared on the email id provided by you at the time of " \
            #                "registration. See you on 26th February. For any query, contact us at " \
            #                "9425827903, 7880094636, 8269199962.&templateid=" + \
            #           template_id + ""

            #     response = requests.request("GET", url, headers={}, data={})

            #     print("===> Result for sending purchase plan: ", response.text)
            # except Exception as e:
            #     print("===> e: ", e)
            #     pass

            messages.success(
                request,
                f'Dear {qry_reg_user} you have successfully registered for BGI TIMELAPSE under {qry_reg_user.category.name} You will get your tickets on whatsapp soon, for more info please contact +91 8109198824!'
            )
            # messages.success(request, "Your registration is successful!")
            # return redirect('/registration/marathon_2')
            return redirect(success_page)

        else:
            return redirect(bhopal_index_view)

    else:
        order_payment_id = json.loads(request.POST.get("error[metadata]")).get(
            "order_id"
        )
        payment_id = json.loads(request.POST.get("error[metadata]")).get(
            "payment_id"
        )

        try:
            qry_order = Order.objects.get(
                order_payment_id=order_payment_id
            )
            qry_order.payment_id = payment_id
            qry_order.save()
        except Order.DoesNotExist:
            # messages.error(request, "Registration is not succeed due to payment issue!")
            return redirect(bhopal_index_view)

        # messages.error(request, "Registration is not succeed due to payment!")
        return redirect(bhopal_index_view)


def bhopal(request):
    return render(request, 'core/bhopal.html')


def indore2(request):
    return render(request, 'core/indore_new.html')


def term_condition(request):
    return render(request, 'core/term_condi.html')


def indore_new(request):
    return render(request, 'core/indore_new.html')


def blog(request):
    return render(request, 'core/blog.html')


def How_To_Register_For_BGITimelapse(request):
    return render(request, 'core/How_To_Register_For_BGITimelapse.html')


def Farhan_Akhtar_Live_At_BGITimelapse(request):
    return render(request, 'core/Farhan_Akhtar_Live_At_BGITimelapse.html')


def Win_Exciting_Prizes_at_BGI_Timelapse(request):
    return render(request, 'core/Win_Exciting_Prizes_at_BGI_Timelapse.html')


def What_is_BGITimelapse(request):
    return render(request, 'core/What_is_BGITimelapse.html')


def Get_Ready_To_Groove_With_Farhan_Akhtar(request):
    return render(request, 'core/Get_Ready_To_Groove_With_Farhan_Akhtar.html')


def Schedule_And_Events_At_BGITimelapse(request):
    return render(request, 'core/Schedule_And_Events_At_BGITimelapse.html')


def Top_5_College_Fests_You_Must_Attend(request):
    return render(request, 'core/Top_5_College_Fests_You_Must_Attend.html')


def razorpay_checks(request):
    # ----- CONNECTING TO RAZORPAY CLIENT ----- #
    if config("DOMAIN_NAME") == "prod":
        client = razorpay.Client(
            auth=(
                config("RAZORPAY_PUBLIC_KEY_PRODUCTION"),
                config("RAZORPAY_SECRET_KEY_PRODUCTION")
            )
        )

    else:
        client = razorpay.Client(
            auth=(
                config("RAZORPAY_PUBLIC_KEY_DEVELOPMENT"),
                config("RAZORPAY_SECRET_KEY_DEVELOPMENT")
            )
        )
    # ----- CONNECTING TO RAZORPAY CLIENT ----- #

    # print("===> request.GET: ", request.GET)
    # print("===> request.GET.get('key'): ", request.GET.get('key'))
    if request.GET.get('key') == config("RAZORPAY_CHECK_KEY"):

        qry_orders = Order.objects.all()

        data = list()
        for qry_order in qry_orders:
            temp = dict()
            order_id = qry_order.order_payment_id

            order = client.order.fetch(order_id)

            if order.get("status") == "paid":
                # MARKING is_paid TRUE FOR ALL RECEIVED PAYMENTS
                qry_order.is_paid = True
                qry_order.save()
                # MARKING is_paid TRUE FOR ALL RECEIVED PAYMENTS OF REGISTERD-USERS
                qry_order.register_user.is_paid = True
                qry_order.register_user.save()
                temp['user_id'] = qry_order.register_user.id
                temp['mobile_number'] = qry_order.register_user.mobile_number
                temp['status'] = True
                # print(f"Payment received for {qry_order}")
            else:
                temp['user_id'] = qry_order.register_user.id
                temp['mobile_number'] = qry_order.register_user.mobile_number
                temp['status'] = False
                # print(f"Payment not received for {qry_order}")

            data.append(temp)

        return JsonResponse(data, safe=False)

    else:
        data = list()
        return JsonResponse(data, safe=False)
