var RES_MUTED_IMG  = "https://sr.knowlarity.com/static/sr/img/common/unmute.png";
var RES_UNMUTED_IMG = "https://sr.knowlarity.com/static/sr/img/common/mute.png";
var RES_ACCEPT_CALL_IMG  = "https://sr.knowlarity.com/static/sr/img/common/accept.png";
var RES_REJECT_CALL_IMG = "https://sr.knowlarity.com/static/sr/img/common/hangup.png";
var RING1 =  "https://webrtc-audio.knowlarity.com/tring_3_times.mp3";
var ping_counter = 1
var img_data = false
var ws_hit = 0
class WebRTCClient {

  constructor() {
    this.state = {}
    this.callbacks = {}
  }

  setUser(userConfig){
    this.userConfig = userConfig
    var self = this
    this.sendEventToSoftphone({type:'set-user',payload:self.userConfig})

  }



  setWebRTCEnabled(status){
    this.userConfig['status'] = status
    var self = this
    this.sendEventToSoftphone({type:'set-user',payload:self.userConfig})
    self.pingStopped=false
    self.pingUser=''
    if(status == true && !this.pingUser){
      self.startPingUser()
    }

  }

  isConnected() {
    if(this.userConfig['status']== true){
      if(this.pingUser ){
        //console.log("[pinguser][pinguser] connected::" + this.pingUser.isConnected())
      }
      if(this.user ){
        //console.log("[pinguser][user] connected::" + this.user.isConnected())
      }
      if(this.pingUser && this.pingUser.isConnected() && this.user && this.user.isConnected()){
        return true

      }else if(!this.pingUser && this.user && this.user.isConnected()){
        return true
      }
    }

    return false;

  }

  /**
   * start a ping user agent , that pings the ws every 30 seconds
   * if REGISTER or CONNECT FAILS it shall trigger the isConnected() to return false
   */
  startPingUser(){
    var TIMEOUT = 10000;

    if(this.pingUser){

      //pingUser is already there and started state...do not create more than one instance
      return
    }
    var userConfig = this.userConfig
    var self = this

    if(userConfig && userConfig.pingUser && ! this.pingUser){
          var response = this.sendEventToSoftphone({type:'set-ping-user',payload:self.userConfig})
          this.pingUser = response['pingUser']
          this.connectAndRegisterPingUser = response['connectAndRegisterPingUser']

          if (!this.pingUser) {
            //console.log("[pinguser] Something went wrong");
            return
          }else{
            //console.log("[pinguser] created..");
          }

          const ping = ()=>{
            setTimeout(
            function(){
              if(self.pingUser){
                  if(self.pingStopped===true)return
                  self.connectAndRegisterPingUser(self.pingUser)
                  //console.log("[pinguser] ping again with wrtcserver...")

              }else{
                //console.log("[pinguser] is null...")
              }

              ping()

            },TIMEOUT)

        }

        ping()

    }
  }

  isWebRTCCall() {
    if(this.userConfig['status']== true){
      //webrtc enabled
      if(!this.user || !this.user.isConnected()){
        //console.log("webrtc enabled, but not connected with server.")
      }

      return true
    }

    return false;

  }

  log() {
    //.log("[WebRTCClient] state:", this.state)
  }

  /**
   *
   * @param {*} eventType
   * @param {*} callback
   * UI / frontend may register for a callback on specifc softphone events
   * Example eventTypes:
   *   invite
   *   register
   *   deregister
   */
  registerCallback(eventType, callback) {
    this.callbacks[eventType] = callback
    //console.log("[WebRTCClient][registerCallback] eventType:", eventType)

  }


  /**
   *
   * @param {*} event
   *
   * Softphone throws various events for frontend to consume.
   * It also invokes automatically any callbacks regsitered  via registerCallback(..) for an event type
   */
  onSoftphoneEvent(event) {
    var self = this
    self.event = event
    self.state[event.type] = event

    //console.log("[WebRTCClient][onSoftphoneEvent] event:", event)
    self.log()

    if (self.event.type == 'add-softphone') {
      self.user = event['payload']['user']
      self.softphone = event['payload']['softphone']

    }

    if (self.event.type == 'registered-pinguser') {
      //self.connect_state = 'registered-pinguser'
      self.pingUser = event['payload']['pingUser']
      self.softphone = event['payload']['softphone']
      //console.log("Ping User Registered Successfully")

    }

    if (self.event.type == 'registered') {
      ToasterWebrtc('show_webrtc green','Webrtc:User Registered Succesfully')
      $('#webrtcModal').show();
      $('.profile_sip_icons .webrtcicon').show()
      $('.profile_sip_icons .webrtcerroricon').hide()
      self.connect_state = 'registered'
      self.user = event['payload']['user']
      self.softphone = event['payload']['softphone']
      if (ping_counter==1 && !window.fetch_turn_cred){
        setTimeOutAfter3()
      }
    }

    if (self.event.type == 'register_error') {
      ToasterWebrtc('show_webrtc red','Webrtc: Unable to register for WebRTC/ Softphone. Kindly retry selecting the calling mode. If the problem persists, reach out to Supervisor or Administrator.')
      $('.profile_sip_icons .webrtcicon').hide()
      $('.profile_sip_icons .webrtcerroricon').show()
      self.connect_state = 'not-connected'
      $('#callInfoContainer').html("")
      this.pingStopped = true
      //self.user = null
      //self.softphone = null
      ping_counter = 1
      ws_hit = 0
      window.clearInterval(img_data)
    }

    if (self.event.type == 'ping_registered') {
      //console.log("[pinguser] WebRTC ping user ping_registered. ")
      self.pingUser = event['payload']['pingUser']
      self.softphone = event['payload']['softphone']
      //console.log("[pinguser] WebRTC ping user ping_registered. ",self.pingUser)

    }

    if (self.event.type == 'ping_register_error') {
      //console.log("[pinguser] WebRTC ping user ping_register_error i.e. Not connected. ")
      //@TODO:pinguser   -- if we want we can add cancel setTimeout here.
      this.pingStopped = true
      $('#callInfoContainer').html("")
      ToasterWebrtc('show_webrtc red','Webrtc: Unable to register for WebRTC/ Softphone. Kindly retry selecting the calling mode. If the problem persists, reach out to Supervisor or Administrator.')
      $('.profile_sip_icons .webrtcicon').hide()
      $('.profile_sip_icons .webrtcerroricon').show()
      window.webRTCClient.sendEventToSoftphone({type:'hangup'})

      //self.connect_state = 'not-connected'
      //self.user = null
      //self.softphone = null

    }

    if (self.event.type == 'unregistered') {
      self.connect_state = 'not-connected'
      //self.user = null
      //self.softphone = null
      if (ws_hit == 0 && ping_counter == 1) {
        ToasterWebrtc('show_webrtc red','Webrtc: Unable to register for WebRTC/ Softphone. Kindly retry selecting the calling mode. If the problem persists, reach out to Supervisor or Administrator.')
        $('.profile_sip_icons .webrtcicon').hide()
        $('.profile_sip_icons .webrtcerroricon').show()
        ping_counter = 1
        window.clearInterval(img_data)
      }
    }

    if (self.event.type == 'disconnected') {
      self.connect_state = 'not-connected'
      //self.user = null
      //self.softphone = null
      $('.profile_sip_icons .webrtcicon').hide()
      $('.profile_sip_icons .webrtcerroricon').show()
      window.webRTCClient.sendEventToSoftphone({type:'hangup'})
      ping_counter = 1
      ws_hit = 0
    }

    if (self.event.type == 'invited') {
    //   if(self.userConfig.playRingtone){
    //      playStream()//play ringtone
    //   }
      console.log("invited!")
    }
    if (self.event.type == 'ongoing') {
      if(self.userConfig.playRingtone){
        pauseStream()//stop ringtone
      }

    }
    if (self.event.type == 'error_accept_reject') {
      if(self.userConfig.playRingtone){
        pauseStream()//stop ringtone
      }
      if (window.hideCallInfo){
        window.hideCallInfo()
      }

    }
    if (self.event.type == 'rejected') {
      if(self.userConfig.playRingtone){
        pauseStream()//stop ringtone
      }

      if (window.hideCallInfo){
        window.hideCallInfo()
      }

    }
    if (self.event.type == 'holded') {

    }
    if (self.event.type == 'unholded') {

    }

    if (self.event.type == 'muted') {
       $('.wrtc-ctrl .mute').addClass('muted')
       $('.wrtc-ctrl .mute img').attr('src',RES_MUTED_IMG)

    }
    if (self.event.type == 'unmuted') {
      $('.wrtc-ctrl .mute').removeClass('muted')
      $('.wrtc-ctrl .mute img').attr('src',RES_UNMUTED_IMG)

    }
    if (self.event.type == 'hangup') {
      if(self.userConfig.playRingtone){
        pauseStream()//stop ringtone
      }

      if (window.hideCallInfo){
        window.hideCallInfo()
      }

    }

    //console.log("[WebRTCClient][] calling callback:: for event:", event.type)
    if (self.callbacks[event.type] != undefined) {
      //console.log("[WebRTCClient][] called callback:: for event:", event.type)
      self.callbacks[event.type](event)
    }
  }

  registerSoftphone(softphone){
    this.softphone = softphone
  }

  /**
   *
   * @param {*} params
   * params is a command map
   * {type:<ongoing|mute|unmute|hold|unhold|hangup|set-user>,payload:{...}}
   */
  sendEventToSoftphone(event) {
    var self = this
    //console.log("[WebRTCClient][sendEventToSoftphone] event:", event)

    if(self.softphone){
      return self.softphone(self.user, event)

    }else{
      self.log()
      //console.log("[WebRTCClient][sendEventToSoftphone] softphone is null")
      return {}
    }
  }

}

var webRTCClient = new WebRTCClient();
window.webRTCClient = webRTCClient

function onSipInvite(event) {
  window.sip_event_type="invited"
}



window.webRTCClient.registerCallback("invited", onSipInvite)

$(function(){
  $('body').on('click','.accept_call',function(){
    //console.log("[WebRTCClient] trigger accept_call....")
    window.detectMicrophone("0")
    window.webRTCClient.sendEventToSoftphone({type:'accept'})
  })

  $('body').on('click','.reject_call',function(){
    //console.log("[WebRTCClient] trigger reject_call....")
    window.webRTCClient.sendEventToSoftphone({type:'reject'})
  })

  $('body').on('click','.wrtc-ctrl .mute',function(e){
    //console.log("[WebRTCClient] trigger mute/unmute....")
    if($(this).hasClass('muted')){
      //console.log("trigger unmute....")
      window.webRTCClient.sendEventToSoftphone({type:'unmute'})

    }else{
        //console.log("trigger mute....")
        window.webRTCClient.sendEventToSoftphone({type:'mute'})
    }
  })

  $('body').on('click','.wrtc-ctrl .hangup',function(e){
    //console.log("[WebRTCClient] trigger hangup....")
    window.webRTCClient.sendEventToSoftphone({type:'hangup'})
  })
  //console.log("[WebRTCClient] mute/unmute /hangup on click registered...")
})

var allow_audio = false
$('.allow_audio').click(function(e) {
  allow_audio = true
});

$('.toggle_ringtone').click(function(e) {
  playStream()
});

function playStream() {

  var player = document.getElementById('ringtone');

  (player.paused == true) ? toggleRingtone(0) : false;

}

function pauseStream() {

  var player = document.getElementById('ringtone');

  (player.paused == true) ? false : toggleRingtone(1);

}

function toggleRingtone(state) {
  //if(!allow_audio)return;

  var player = document.getElementById('ringtone');
  var src = RING1

  switch(state) {
          case 0:
                  player.src = src;
                  player.load();
                  player.play();
                  break;
          case 1:
                  player.pause();
                  player.currentTime = 0;
                  player.src = '';
                  break;
  }
}

function ToasterWebrtc(webrctClass,webrctText) {
  var x = document.getElementById("toaster_webrtc")
  x.innerHTML=webrctText
  x.className = webrctClass;
  setTimeout(function(){ x.className = x.className.replace(webrctClass, ""); }, 6000);
}

function setTimeOutAfter3(){
  ping_counter += 1
  loop_count = 0
  self = this
  error_hit = false
  img_data=setInterval(function(){
    $.ajax({
      timeout: 3000,
      url: "https://" + window.kamailio_domain + "/manifest.json",
      cache: false,
      method: "OPTIONS",
      success: function(data) {
        loop_count = 0
        if (error_hit == true){
          window.webRTCClient.setWebRTCEnabled(true)
          error_hit = false
        }
      },
      error: function(jqXHR, textStatus) {
        if (jqXHR.status === 0 && jqXHR.statusText === 'error'){
          // CORS handling
          loop_count = 0
        } else{
          error_hit = true
          window.webRTCClient.setWebRTCEnabled(false)
          loop_count++;
          if (textStatus === 'timeout') {
            console.log("Ping Disconnected")
          }
          if (loop_count >= 2) {
            clearInterval(img_data)
            img_data = false
            ToasterWebrtc('show_webrtc red', 'Webrtc: Unable to register for WebRTC/ Softphone. Kindly retry selecting the calling mode. If the problem persists, reach out to Supervisor or Administrator.')
            $('.profile_sip_icons .webrtcicon').hide()
            $('.profile_sip_icons .webrtcerroricon').show()
            $('#callInfoContainer').html("")
            ping_counter = 1
            ws_hit = 0
          }
        }
      }
    });
  }, 10000)
}